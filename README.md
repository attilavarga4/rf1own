# Angular 2 CLI - LoopBack 3.x CRUD leírás

Az alábbi projekt egy minta arra, hogy az Angular 2 és a LoopBack 3.x hogyan működik együtt, illetve hogyan tudunk CRUD műveleteket végrehajtani Service-ekben. Az alábbi lépéseket nem kell végrehajtani, a projektben ezek mind megvalósultak. Ha új projektet szeretnénk készíteni, akkor kövessük a lent található lépéseket.
A futtatáshoz szükség van az alábbiakra:
* Node.JS (v8.9.3 LTS) / NPM (v5.5.1)

``` https://nodejs.org/en/download/current/ ```

* LoopBack CLI (v3.x)

``` npm install -g loopback-cli ```

* Typescript Compiler (v2.6.2)

``` npm install -g typescript ```

* Angular 2 CLI (v1.6)

``` npm install -g @angular/cli ```

Ha ezek fel vannak telepítve, akkor az app az alábbi módon futtatható:
1. a projekt mappájában (ahol látható a webapp és server mappa is), futtassuk le az npm install parancsot
2. a webapp mappában futtassuk le az npm install parancsot
3. a projekt mappájában (ahol látható a webapp és a server mappa is), futtassuk a node . parancsot
4. a webapp mappában futtassuk az npm start parancsot

## LoopBack 3.x inicializálása (új projekt esetén)

* Lépés 1: Az alkalmazáshoz egy LoopBack API-t generálunk a LoopBack generátor segítségével.

```sh
$ lb app app_name
```

### Innenstől fontos az rf1-lb projekt részhez.

* Lépés 2: Ha szükséges új model létrehozása, akkor azt is generáljuk le!

```sh
$ lb model model_name
```

### Innenstől fontos az rf1-angular projekt részhez.

* Lépés 3: Generáljuk le az Angular 2 SDK-t!
https://github.com/mean-expert-official/loopback-sdk-builder/wiki/1.-Install-Builder-&-Build-SDK

```sh
$ cd to/loopback/project
$ npm install --save @mean-expert/loopback-sdk-builder
$ npm install --save @mean-expert/loopback-component-realtime

$ ./node_modules/.bin/lb-sdk server/server.js ./webapp/sdk -d ng2web -i enabled
```

> Megjegyzés: Windows alatt a parancssorban(cmd-ben, powershellbe így is jó) a ./node_modules/... hívásokban cseréljük le a / jeleket \ jelekre! HA NEM MŰKÖDIK NG SERVE-KOR AKKOR A FŐKÖNYVTÁRBAN HELPER-TOOLS/RXJS-FIX BŐL AZ SDK MAPPÁVALVAL FELÜL KELL ÍRNI AZ SDK MAPPÁT AZ ANGULAR PROJEKTBEN!!!



* Lépés 5: A generált SDK-t (sdk mappa a webapp mappában) tegyük bele egy új shared mappába! A shared mappát tegyük ugyanabba a mappába, ahol az app.module.ts található!


* Lépés 6. Telepítsük a socket.io-client modult!

```sh
$ npm install --save socket.io-client
$ npm install @types/socket.io-client --save
```


* Lépés 7: Importáljuk a socket.io-client modult a main.ts fájlba (kliensnél)!

```javascript
import * as io from 'socket.io-client';
```
* Lépés 8: Importáljuk az SDKBrowserModule-t, és hívjuk meg a forRoot() metódusát, hogy az SDK Singleton-jait elérjük!

```javascript
import { SDKBrowserModule } from './shared/index';

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule, MenuModule, CrudModule, RouterModule.forRoot(appRoutes), SDKBrowserModule.forRoot() ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
```

* Lépés 9: A LoopBackConfig komponenst inicializálnunk kell! Ehhez importáljuk be a főkomponensünkbe (app.component.ts), majd a konstruktorban inicializáljuk!

```javascript
import { LoopBackConfig } from '../shared/index';

constructor() {
	LoopBackConfig.setBaseURL('//localhost:3000');
	LoopBackConfig.setApiVersion('api');
}
```

## LoopBack 3.x használata Service-ben (új projekt esetén)


* Lépés 1: Importáljuk be a generált Api komponenst (pl.: UserApi) a megfelelő Service-be!

```javascript
import { UserApi } from '......./shared/index';
```

> Megjegyzés: Érdemes az index.ts-t beimportálni minden Api esetén, mert az fogja össze az összes model-t, és innen minden elérhető lesz!


* Lépés 2: Példányosítsuk az Api-nkat a Service konstruktorában!

```javascript
constructor(private userApi: UserApi) { }
```
> (ha a UserApi-val szeretnénk dolgozni)


* Lépés 3: Írjuk meg a konkrét kérést! (Pl.: egy User-ekre vonatkozó GET kérés így néz ki...)

```javascript
getUsers(): Observable<any> {
	return this.userApi.find();
}
```


* Lépés 4: A Service komponensében példányosítsunk egy Service objektumot (konstruktorban)! (Pl.: ha az előbbi UserApi-hoz írtunk Service-t, akkor lehet akár UsersService a neve)

```javascript
constructor(private _crudService: CrudService) { }
```


* Lépés 5: Hívjuk meg a megfelelő helyen a Service objektum metódusát (gyakran ezt az ngOnInit() függvényben tesszük meg, így biztosan rendelkezésre állnak a kapott objektumok, mielőtt bármit is csinálna a felhasználó a felületen)!

```javascript
ngOnInit() {
    this._crudService.getUsers().subscribe((data) => {
      data.forEach(element => {
        console.log(element);
      });
    });
}      
```

> Megjegyzés: a Service metódusának (getUsers()) visszatérési értékére meghívjuk a subscribe() metódust, mivel Observable-lel tértünk vissza. A callback-ben kezeljük a visszatért data tömböt, majd azt bejárjuk, és az elemeket egyenként kezeljük (ha több is van, márpedig User-ből több is szokott lenni). A data-tól kezdve már JSON objektumokkal dolgozunk!


* Lépés 6: A LoopBack alapból autentikációval kezel minden műveletet, így ha ilyesmi nincs beállítva, akkor ezt tiltsuk le!
.../myProject/server/boot/authentication.js

Az alábbi sort kommentezzük ki!

```javascript
server.enableAuth();
```


* Lépés 7: A teszteléshez indítsuk el a szervert! Menjünk a projekt mappába, majd adjuk ki a node . parancsot!


* Lépés 8: A kliens oldali alkalmazást futtassuk az npm start paranccsal!
