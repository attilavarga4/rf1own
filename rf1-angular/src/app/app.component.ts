import { Component } from '@angular/core';
import { LoopBackConfig } from './shared/sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor() {
    LoopBackConfig.setBaseURL('//localhost:3000');
    LoopBackConfig.setApiVersion('api');
  }
}
