'use strict';

var installed = true;
module.exports = function(app) {
	  var User = app.models.Customer;
    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;

 RoleMapping.belongsTo(User);
 User.hasMany(RoleMapping, {foreignKey: 'principalId'});
 Role.hasMany(User, {through: RoleMapping, foreignKey: 'roleId'});
//{"username": "adminuser", "password": "admin"}
//{"username": "customeruser", "password": "customer"}
  
console.log('install running');

   if (!installed) {
    console.log('install enter');
    User.create([
      {username: 'adminuser', email: 'admin@admin.com', password: 'admin', 'firstname': 'adminfirst', 'lastname': 'adminlast'},
      {username: 'customeruser', email: 'customer@customer.com', password: 'customer', 'firstname': 'customerfirst', 'lastname': 'customerlast'},
      {username: 'customeruse2r', email: 'customer2@customer.com', password: 'customer2', 'firstname': 'customer2first', 'lastname': 'customer2last'}
    ], function (err, users) {
      if (err) throw err;


      console.log("Created User: ", users);
      //create the admin role
      Role.create({
        name: 'administrator'
      }, function (err, role) {
        if (err) throw err;

        //make adminuser an admin
        role.principals.create({
          principalType: RoleMapping.USER,
          principalId: users[0].id
        }, function (err, principal) {
          console.log('Created principal:', principal);
        });
      });

      //create the customer role
      Role.create({
        name: 'customer'
      }, function (err, role) {
        if (err) throw err;

        //make customeruser a customer
        role.principals.create({
          principalType: RoleMapping.USER,
          principalId: users[1].id
        }, function (err, principal) {
          console.log('Created principal:', principal);
        });
        role.principals.create({
          principalType: RoleMapping.USER,
          principalId: users[2].id
        }, function (err, principal) {
          console.log('Created principal:', principal);
        });
      });

    });
}

};